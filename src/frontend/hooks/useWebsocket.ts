import { useEffect, useCallback } from 'react'
import useAppContext from './useAppContext'
import { WsMessage, User, Message } from 'shared/types'

const replace = <T extends User | Message>(
  elements: T[],
  newElement: T
): T[] => {
  const newElements = [...elements]
  const index = newElements.findIndex((element) => element.id === newElement.id)
  newElements.splice(index, 1, newElement)
  return newElements
}

const useWebsocket = () => {
  const [{ websocket, users, messages }, setState] = useAppContext()

  const listener = useCallback(
    ({ data }: MessageEvent) => {
      const wsMessage = JSON.parse(String(data)) as WsMessage
      console.log('Websocket message: ', wsMessage)
      const { type, content } = wsMessage

      if (['user', 'users', 'messages'].includes(type)) {
        setState({ [type]: content })
      } else if (type === 'newMessage') {
        const { author } = content
        setState({ messages: [...messages, content] })
        setState({ users: replace(users, author) })
      } else if (type === 'updateMessage') {
        setState({ messages: replace(messages, content) })
      }
    },
    [users, messages, setState]
  )

  useEffect(() => {
    if (websocket) {
      websocket.addEventListener('message', listener)
      return () => websocket.removeEventListener('message', listener)
    }
  }, [websocket, listener])
}

export default useWebsocket

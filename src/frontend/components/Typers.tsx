import { useRef, useEffect, FunctionComponent } from 'react'
import { useAppContext, useScrollContext, useDeferUpdates } from 'hooks'
import { User } from 'shared/types'
import { LOCALE } from 'shared/constants'
import './Typers.css'

type Typer = {
  name: string
  expires: number
  unmounts: number
}

const EXPIRES = 5 * 1000

const formatter = new Intl.ListFormat(LOCALE, {
  style: 'long',
  type: 'conjunction',
})

const isKeyPresser = (user: User): user is Required<User> => !!user.lastKeyPress

const Typers: FunctionComponent = () => {
  const [{ users }] = useAppContext()
  const { updateScroll } = useScrollContext()
  const [update, deferUpdates] = useDeferUpdates()
  const containerRef = useRef<HTMLDivElement>(null)
  const now = Date.now()

  const typers = users.filter(isKeyPresser).map(({ name, lastKeyPress }) => ({
    name,
    expires: lastKeyPress + EXPIRES,
    unmounts: lastKeyPress + EXPIRES + 500,
  })) as Typer[]

  useEffect(() => {
    deferUpdates(
      typers.map(({ expires, unmounts }) => [expires, unmounts]).flat()
    )

    if (
      typers.some(({ expires, unmounts }) => expires <= now && unmounts > now)
    ) {
      const height = containerRef.current?.offsetHeight ?? 0
      updateScroll(-(height + 5))
    } else {
      updateScroll()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [users, update])

  const mountedTypers = typers
    .filter(({ unmounts }) => unmounts > now)
    .map(({ name }) => name)

  if (!mountedTypers.length) {
    return null
  }

  return (
    <div ref={containerRef} className="Typers">
      {formatter.format(mountedTypers) + ' kirjoittaa'}
    </div>
  )
}

export default Typers

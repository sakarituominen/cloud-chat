import { createContext } from 'react'
import { ScrollContextValue } from 'shared/types'

const scrollContext = createContext<ScrollContextValue>({
  updateScroll: () => {},
})

export default scrollContext

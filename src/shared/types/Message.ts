import User from './User'
import Metadata from './Metadata'

type Message = {
  id: string
  author: User
  content: string
  metadata: Metadata[]
  sent: number
  updated: number
}

export default Message

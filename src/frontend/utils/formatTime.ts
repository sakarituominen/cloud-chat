import { LOCALE } from 'shared/constants'

const formatTime = (
  date: string | number,
  timeStyle: Intl.DateTimeFormatOptions.timeStyle = 'short'
): string => new Date(date).toLocaleTimeString(LOCALE, { timeStyle })

export default formatTime

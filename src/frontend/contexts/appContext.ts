import { createContext } from 'react'
import { AppState, StateContextValue } from 'shared/types'

export const defaultState: AppState = {
  websocket: undefined,
  user: undefined,
  users: [],
  messages: [],
}

const appContext = createContext<StateContextValue<AppState>>([
  defaultState,
  () => undefined,
])

export default appContext

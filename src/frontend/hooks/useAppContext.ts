import { useContext } from 'react'
import { appContext } from 'contexts'

const useAppContext = () => useContext(appContext)

export default useAppContext

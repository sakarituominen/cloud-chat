import express from 'express'
import expressWs from 'express-ws'
import crypto from 'crypto'
import { getMetadata } from './metadata'
import { User, Message, WsMessage } from 'shared/types'

const port = process.env.PORT ?? 3001

const { app, getWss } = expressWs(express())

const users: User[] = []
const messages: Message[] = []

const log = (...args: unknown[]) => console.log(new Date(), ...args)

const keyPress = (user: User, time?: number) => {
  user.lastKeyPress = time
  user.updated = Date.now()
}

const broadcast = (wsMessage: WsMessage) => {
  const { clients } = getWss()
  log('Broadcast to', clients.size, 'clients:', wsMessage)
  clients.forEach((client) => client.send(JSON.stringify(wsMessage)))
}

app.ws('/api/:name', (ws, req) => {
  const { ip } = req
  const { name } = req.params

  log('New connection from', ip)

  const send = (wsMessage: WsMessage) => {
    log('Send to client:', wsMessage)
    ws.send(JSON.stringify(wsMessage))
  }

  if (users.some((user) => user.name === name)) {
    send({ type: 'error', content: 'Username already taken.' })
    return
  }

  const user: User = {
    id: crypto.randomUUID(),
    name,
    lastKeyPress: undefined,
    updated: Date.now(),
  }

  users.push(user)

  broadcast({ type: 'users', content: users })
  send({ type: 'user', content: user })
  send({ type: 'messages', content: messages })

  ws.on('close', () => {
    log('Connection closed from', ip)
    const userIndex = users.indexOf(user)
    users.splice(userIndex, 1)
    broadcast({ type: 'users', content: users })
  })

  ws.on('message', async (data) => {
    const wsMessage = JSON.parse(data.toString()) as WsMessage
    const { type, content } = wsMessage

    log('Message from client:', { type, content })

    if (type === 'keyPress') {
      keyPress(user, Date.now())
      broadcast({ type: 'users', content: users })
    } else if (type === 'sendMessage') {
      keyPress(user)

      const message: Message = {
        id: crypto.randomUUID(),
        author: user,
        content,
        metadata: [],
        sent: Date.now(),
        updated: Date.now(),
      }

      messages.push(message)

      while (messages.length > 100) {
        messages.shift()
      }

      broadcast({ type: 'newMessage', content: message })

      const metadata = await getMetadata(message)

      if (metadata) {
        Object.assign(message, { metadata, updated: Date.now() })
        broadcast({ type: 'updateMessage', content: message })
      }
    }
  })
})

if (process.env.NODE_ENV === 'production') {
  app.use(express.static('build/frontend'))
}

app.listen(port, () => console.log(`Server running on port ${port}`))

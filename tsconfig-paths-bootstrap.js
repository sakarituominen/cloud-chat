const tsConfig = require('./src/tsconfig.json')
const tsConfigPaths = require('tsconfig-paths')

const baseUrl = 'build/server'

tsConfigPaths.register({
  baseUrl,
  paths: tsConfig.compilerOptions.paths,
})

import Messages from './Messages'
import Users from './Users'
import MessageForm from './MessageForm'
import { useAppContext } from 'hooks'
import './Chat.css'

const Chat = () => {
  const [{ user }] = useAppContext()

  if (!user) {
    return null
  }

  return (
    <div className="Chat">
      <section className="view">
        <Messages />
        <Users />
      </section>
      <section className="form">
        <MessageForm />
      </section>
    </div>
  )
}

export default Chat

import { useRef, useState, FunctionComponent, FormEvent } from 'react'
import { getValues } from 'utils'
import { useAppContext } from 'hooks'
import { WsMessage } from 'shared/types'
import './MessageForm.css'

const MessageForm: FunctionComponent = () => {
  const [{ websocket }] = useAppContext()
  const formRef = useRef<HTMLFormElement>(null)
  const keyPressedRef = useRef<number>()
  const [disabled, setDisabled] = useState(false)

  const send = (wsMessage: WsMessage) =>
    websocket?.send(JSON.stringify(wsMessage))

  const onSubmit = (event: FormEvent) => {
    event.preventDefault()

    setDisabled(true)

    const { message } = getValues(formRef)
    send({ type: 'sendMessage', content: message })

    formRef.current?.reset()

    setDisabled(false)
  }

  const onKeyPress = () => {
    if (!keyPressedRef.current || keyPressedRef.current < Date.now() - 1000) {
      keyPressedRef.current = Date.now()
      send({ type: 'keyPress', content: null })
    }
  }

  return (
    <form ref={formRef} className="MessageForm" onSubmit={onSubmit}>
      <fieldset disabled={disabled}>
        <input
          name="message"
          maxLength={1000}
          required
          autoComplete="off"
          autoCapitalize="on"
          autoFocus
          onKeyPress={onKeyPress}
        />
        <button type="submit">Lähetä</button>
      </fieldset>
    </form>
  )
}

export default MessageForm

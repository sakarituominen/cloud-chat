import ogs from 'open-graph-scraper'
import { Message, Metadata } from 'shared/types'
import { URL_MATCHER } from 'shared/constants'

const fetchMetadata = async (url: string): Promise<Metadata | undefined> => {
  const { origin } = new URL(url)

  try {
    const {
      result: {
        success,
        ogSiteName: siteName,
        ogTitle: title,
        ogDescription: description,
        ogDate: date,
        ogImage,
        favicon,
      },
    } = await ogs({ url })

    if (!success) {
      throw new Error('Fetch error')
    }

    const iconUrl = favicon && String(new URL(favicon, origin))
    const [image] = Array.isArray(ogImage) ? ogImage : [ogImage]
    const imageUrl = typeof image === 'object' ? image.url : undefined

    const content = {
      url,
      iconUrl,
      siteName,
      title,
      description,
      date,
      imageUrl,
    }

    return { type: 'url', content }
  } catch (error) {
    console.error('Could not scrape:', url)
    return undefined
  }
}

export const getMetadata = async (message: Message) => {
  const { content } = message
  const matches = content.match(URL_MATCHER)

  if (!matches) {
    return
  }

  const uniqueMatches = matches.filter(
    (match, index, array) => array.indexOf(match) === index
  )

  const metadata = (await Promise.all(uniqueMatches.map(fetchMetadata))).filter(
    Boolean
  ) as Metadata[]

  return metadata.length ? metadata : undefined
}

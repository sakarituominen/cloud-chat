import User from './User'
import Message from './Message'

type WsMessage =
  | {
      type: 'user'
      content: User
    }
  | {
      type: 'users'
      content: User[]
    }
  | {
      type: 'messages'
      content: Message[]
    }
  | {
      type: 'sendMessage'
      content: string
    }
  | {
      type: 'newMessage'
      content: Message
    }
  | {
      type: 'updateMessage'
      content: Message
    }
  | {
      type: 'keyPress'
      content: null
    }
  | {
      type: 'error'
      content: string
    }

export default WsMessage

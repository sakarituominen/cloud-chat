import { FunctionComponent } from 'react'
import SafeImage from './SafeImage'
import { useScrollContext } from 'hooks'
import { formatDate } from 'utils'
import { Metadata } from 'shared/types'
import './Link.css'

type Props = {
  metadata: Metadata
}

const Link: FunctionComponent<Props> = ({ metadata }) => {
  const { content } = metadata
  const { url, iconUrl, siteName, title, description, imageUrl, date } = content
  const { updateScroll } = useScrollContext()
  const onLoad = () => updateScroll()

  return (
    <div className="Link">
      {siteName && (
        <div className="siteName">
          {iconUrl && (
            <SafeImage src={iconUrl} alt={siteName} onLoad={onLoad} />
          )}
          {siteName}
        </div>
      )}
      <div className="title">
        <a href={url}>{title}</a>
      </div>
      {description && <div>{description}</div>}
      {date && <div className="date">{formatDate(date, 'long')}</div>}
      {imageUrl && (
        <div className="image">
          <a href={url}>
            <SafeImage src={imageUrl} alt={title} onLoad={onLoad} />
          </a>
        </div>
      )}
    </div>
  )
}

export default Link

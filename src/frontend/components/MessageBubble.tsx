import { FunctionComponent, ReactChild } from 'react'
import Link from './Link'
import { formatTime } from 'utils'
import { URL_MATCHER } from 'shared/constants'
import { User, Message } from 'shared/types'
import './MessageBubble.css'

type Props = {
  user: User
  message: Message
}

const formatContent = (content: string): ReactChild[] => {
  const parts = content.split(URL_MATCHER).filter(Boolean)
  return parts.map((part, index) =>
    /^https?:\/\//.test(part) ? (
      <a key={`url-${index}`} href={part}>
        {part}
      </a>
    ) : (
      part
    )
  )
}

const MessageBubble: FunctionComponent<Props> = ({ user, message }) => {
  const { author, content, metadata, sent } = message
  return (
    <div className={'MessageBubble' + (author.id === user.id ? ' my' : '')}>
      <div className="header">
        <div className="author">{author.name}</div>
        <div className="sent">{formatTime(sent)}</div>
      </div>
      <div className="content">{formatContent(content)}</div>
      {metadata.map((metadata, index) => (
        <Link key={`metadata-${index}`} metadata={metadata} />
      ))}
    </div>
  )
}

export default MessageBubble

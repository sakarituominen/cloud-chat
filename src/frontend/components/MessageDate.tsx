import { FunctionComponent } from 'react'
import { formatDate } from 'utils'
import { Message } from 'shared/types'
import './MessageDate.css'

type Props = {
  message: Message
  previousMessage?: Message
}

const MessageDate: FunctionComponent<Props> = ({
  message,
  previousMessage,
}) => {
  const dateString = formatDate(message.sent, 'long')

  if (previousMessage) {
    const previousDateString = formatDate(message.sent, 'long')
    if (previousDateString === dateString) {
      return null
    }
  }

  return (
    <div className="MessageDate">
      <div className="date">{dateString}</div>
    </div>
  )
}

export default MessageDate

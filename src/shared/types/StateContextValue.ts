type StateContextValue<StateT extends object> = [
  StateT,
  (partialState: Partial<StateT>) => void
]

export default StateContextValue

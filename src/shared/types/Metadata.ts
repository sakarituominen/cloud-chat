type Metadata = {
  type: 'url'
  content: {
    url: string
    iconUrl?: string
    siteName?: string
    title?: string
    description?: string
    date?: string
    imageUrl?: string
  }
}

export default Metadata

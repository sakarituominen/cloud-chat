import { LOCALE } from 'shared/constants'

const formatDate = (
  date: string | number,
  dateStyle: Intl.DateTimeFormatOptions.timeStyle = 'short'
): string => new Date(date).toLocaleDateString(LOCALE, { dateStyle })

export default formatDate

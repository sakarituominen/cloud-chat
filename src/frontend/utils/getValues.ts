import { MutableRefObject } from 'react'

const getValues = <T extends object = Record<string, any>>(
  formRef: MutableRefObject<HTMLFormElement | null>
): T => {
  if (!formRef.current) {
    throw new Error('Form ref nonexistent')
  }

  const formData = new FormData(formRef.current)
  const values = Object.fromEntries(formData.entries()) as T

  return values
}

export default getValues

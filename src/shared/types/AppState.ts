import User from './User'
import Message from './Message'

type AppState = {
  websocket?: WebSocket
  user?: User
  users: User[]
  messages: Message[]
}

export default AppState

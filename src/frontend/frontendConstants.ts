export * from 'shared/constants'

/* eslint-disable no-restricted-globals */
export const PROTOCOL = location.protocol === 'https:' ? 'wss:' : 'ws:'

const PORT = process.env.NODE_ENV === 'production' ? location.port : '3001'
export const HOST = `${location.hostname}:${PORT}`

import UserForm from './UserForm'
import Chat from './Chat'
import { useWebsocket } from 'hooks'
import './App.css'

const App = () => {
  useWebsocket()

  return (
    <div className="App">
      <UserForm />
      <Chat />
    </div>
  )
}

export default App

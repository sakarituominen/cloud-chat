import { useState, useRef } from 'react'

const useDeferUpdates = (): [number, (times: number[]) => void] => {
  const updatesRef = useRef<Record<number, number>>({})
  const [update, setUpdate] = useState(Date.now())

  const deferUpdates = (times: number[]) => {
    const { current: updates } = updatesRef
    const now = Date.now()

    times
      .filter((time) => time >= now && !(time in updates))
      .forEach((time) => {
        updates[time] = setTimeout(() => {
          setUpdate(time)
          delete updates[time]
        }, time - now)
      })
  }

  return [update, deferUpdates]
}

export default useDeferUpdates

import { useEffect } from 'react'
import { useAppContext, useDeferUpdates } from 'hooks'
import './Users.css'

const IDLE_TIMEOUT = 30 * 1000

const Users = () => {
  const [{ users }] = useAppContext()
  const [, deferUpdates] = useDeferUpdates()
  const now = Date.now()

  useEffect(
    () => deferUpdates(users.map(({ updated }) => updated + IDLE_TIMEOUT)),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [users]
  )

  return (
    <section className="Users">
      {users.map(({ name, updated }) => (
        <div key={name} className="user">
          <div className="name">{name}</div>
          <div
            className={
              'status ' + (updated + IDLE_TIMEOUT > now ? 'online' : 'idle')
            }
          ></div>
        </div>
      ))}
    </section>
  )
}

export default Users

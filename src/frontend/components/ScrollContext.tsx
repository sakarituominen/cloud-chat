import { FunctionComponent, PropsWithChildren } from 'react'
import { scrollContext } from 'contexts'
import { ScrollContextValue } from 'shared/types'

type Props = {
  value: ScrollContextValue
}

const ScrollContext: FunctionComponent<PropsWithChildren<Props>> = ({
  value,
  children,
}) => {
  const { Provider } = scrollContext
  return <Provider value={value}>{children}</Provider>
}

export default ScrollContext

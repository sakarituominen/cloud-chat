type User = {
  id: string
  name: string
  lastKeyPress?: number
  updated: number
}

export default User

import { useContext } from 'react'
import { scrollContext } from 'contexts'

const useScrollContext = () => useContext(scrollContext)

export default useScrollContext

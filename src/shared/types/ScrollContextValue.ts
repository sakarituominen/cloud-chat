type ScrollContext = {
  updateScroll: (offset?: number) => void
}

export default ScrollContext

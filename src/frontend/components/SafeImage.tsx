import { useState, useEffect, FunctionComponent } from 'react'
import './SafeImage.css'

type Props = {
  src: string
  alt?: string
  onLoad?(): void
}

const SafeImage: FunctionComponent<Props> = ({ src, alt, onLoad }) => {
  const [loaded, setLoaded] = useState<boolean>()

  useEffect(() => {
    const image = new Image()
    image.src = src
    image.addEventListener('load', () => setLoaded(true))
    image.addEventListener('error', () => setLoaded(false))
  }, [src])

  return loaded ? (
    <img src={src} alt={alt} className="SafeImage" onLoad={onLoad} />
  ) : null
}

export default SafeImage

import { useEffect, useRef, useMemo } from 'react'

const useAutoScroll = (content: unknown[]) => {
  const containerRef = useRef<HTMLElement>(null)
  const contentRef = useRef<HTMLDivElement>(null)
  const attachedRef = useRef(true)

  const { onWheel, updateScroll } = useMemo(() => {
    const getScrollTop = (): number => {
      if (containerRef.current && contentRef.current) {
        const { offsetHeight: containerHeight } = containerRef.current
        const { offsetHeight: contentHeight } = contentRef.current
        return Math.max(contentHeight - containerHeight, 0)
      }
      return 0
    }

    const onWheel = () => {
      if (containerRef.current) {
        attachedRef.current =
          Math.abs(containerRef.current.scrollTop - getScrollTop()) <= 10
      }
    }

    const updateScroll = (offset = 0) => {
      if (containerRef.current && attachedRef.current) {
        containerRef.current.scrollTop = getScrollTop() + offset
      }
    }

    return { onWheel, updateScroll }
  }, [])

  useEffect(() => {
    updateScroll()

    if (containerRef.current) {
      const { style } = containerRef.current
      // Set scrollBehavior after content has been loaded
      if (content.length && style.scrollBehavior !== 'smooth') {
        style.scrollBehavior = 'smooth'
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [content])

  useEffect(() => {
    const listener = () => updateScroll()
    window.addEventListener('resize', listener)
    return () => window.removeEventListener('resize', listener)
  }, [updateScroll])

  return { containerRef, contentRef, onWheel, updateScroll }
}

export default useAutoScroll

import { useRef, useState, FunctionComponent, FormEvent } from 'react'
import { getValues } from 'utils'
import { useAppContext } from 'hooks'
import { PROTOCOL, HOST } from 'frontendConstants'
import './UserForm.css'

const UserForm: FunctionComponent = () => {
  const formRef = useRef(null)
  const [disabled, setDisabled] = useState(false)
  const [{ user }, setState] = useAppContext()

  if (user) {
    return null
  }

  const onSubmit = async (event: FormEvent) => {
    event.preventDefault()

    setDisabled(true)

    const { username } = getValues(formRef)
    const websocket = new WebSocket(`${PROTOCOL}//${HOST}/api/${username}`)

    setState({ websocket })

    setDisabled(false)
  }

  return (
    <form ref={formRef} className="UserForm" onSubmit={onSubmit}>
      <fieldset disabled={disabled}>
        <label htmlFor="username">Nimimerkki:</label>
        <input
          name="username"
          id="username"
          size={20}
          maxLength={20}
          required
          autoComplete="off"
          autoFocus
        />
        <button type="submit">Jatka</button>
      </fieldset>
    </form>
  )
}

export default UserForm

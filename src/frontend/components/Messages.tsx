import { Fragment } from 'react'
import { useAppContext, useAutoScroll } from 'hooks'
import ScrollContext from './ScrollContext'
import MessageDate from './MessageDate'
import MessageBubble from './MessageBubble'
import Typers from './Typers'
import './Messages.css'

const Messages = () => {
  const [{ user, messages }] = useAppContext()
  const { containerRef, contentRef, onWheel, updateScroll } =
    useAutoScroll(messages)

  if (!user) {
    return null
  }

  return (
    <section ref={containerRef} className="Messages" onWheel={onWheel}>
      <div ref={contentRef} className="messages">
        <ScrollContext value={{ updateScroll }}>
          {messages.map((message, index) => {
            const { id, updated } = message
            return (
              <Fragment key={`${id}-${updated}`}>
                <MessageDate
                  message={message}
                  previousMessage={messages[index - 1]}
                />
                <MessageBubble user={user} message={message} />
              </Fragment>
            )
          })}
          <Typers />
        </ScrollContext>
      </div>
    </section>
  )
}

export default Messages

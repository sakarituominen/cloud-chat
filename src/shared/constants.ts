export const LOCALE = 'fi'

// From: https://stackoverflow.com/a/3809435
export const URL_MATCHER =
  /(https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b[-a-zA-Z0-9()@:%_\+.~#?&//=]*)/gi

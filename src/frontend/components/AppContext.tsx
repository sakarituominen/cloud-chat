import { useState, FunctionComponent, PropsWithChildren } from 'react'
import appContext, { defaultState } from 'contexts/appContext'
import { AppState } from 'shared/types'

const AppContext: FunctionComponent<PropsWithChildren> = ({ children }) => {
  const { Provider } = appContext
  const [state, setState] = useState<AppState>(defaultState)
  const setPartialState = (partialState: Partial<AppState>) =>
    setState((currentState) => ({ ...currentState, ...partialState }))

  return <Provider value={[state, setPartialState]}>{children}</Provider>
}

export default AppContext
